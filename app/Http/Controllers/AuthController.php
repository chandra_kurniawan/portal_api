<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function index()
    {
        return view('auth.login');
    }


    public function register(Request $request)
    {

        // $url='https://wso2-wso2-system.apps.lab.i3datacenter.my.id/client-registration/v0.17/register';
        // $fields=[
        //     "callbackUrl"=>"www.google.lk",
        //     "clientName"=>"rest_api_devportal",
        //     "owner"=>"admin",
        //     "grantType"=>"client_credentials password refresh_token",
        //     "saasApp"=>true
        // ];

        $reg = Http::withoutVerifying()
            ->withHeaders(['Authorization' => 'Basic '.base64_encode('admin:admin')])
            ->withOptions(["verify"=>false])
            ->post('https://wso2-wso2-system.apps.lab.i3datacenter.my.id/client-registration/v0.17/register',[
                "callbackUrl"=>"www.google.lk",
                "clientName"=>"rest_api_devportal",
                "owner"=>"admin",
                "grantType"=>"client_credentials password refresh_token",
                "saasApp"=>true
            ]);

        $data['reg']=$reg->object();

        $req_token = Http::withoutVerifying()
            ->withHeaders([
                'Authorization' => 'Basic WWxfd29GcGhkS2FhdFVrdGpwVWEwZmh6VHg0YTpCZmg1ZVp5MHBVbEFtblI1T0ZUU3IyRUhvNVlh',
            ])
            ->withOptions(["verify"=>false])
            // ->post('https://wso2-gateway-wso2-system.apps.lab.i3datacenter.my.id/token',[
            //     "query"=>[
            //         "grant_type"=>"password",
            //         "username "=>"admin",
            //         "password "=>"admin",
            //         "scope"=>"apim:api_key apim:app_import_export apim:app_manage apim:store_settings apim:sub_alert_manage apim:sub_manage apim:subscribe openid"
            //     ]
            // ])
            ->asForm()
            ->post('https://wso2-gateway-wso2-system.apps.lab.i3datacenter.my.id/token?grant_type=password&username=admin&password=admin&scope=apim:api_key apim:app_import_export apim:app_manage apim:store_settings apim:sub_alert_manage apim:sub_manage apim:subscribe openid');

        $token=$req_token->object();
        $data['token']=$token;
                // echo "<pre>";
                // print_r($token);
                // die();
        // $tes=$token->getBody()->getContents();
        // $responseData = json_decode($tes, true);

        // $data['ok']=$token->ok();
        // $data['successful']=$token->successful();
        // $data['status']=$token->status();
        // $data['failed']=$token->failed();
        // $data['header']=$token->header('content-type');
        // $data['headers']=$token->headers();
        // $data['token']=$token->object();
        // $data['token_a']=$token->getBody();
        // $data['token_b']=$token->getBody()->getContents();
        // $data['token_c']=$token->body();
        

        $gen_token = Http::withoutVerifying()
            ->withHeaders([
                'Authorization' => 'Basic ODRxS1kxaGVCUW9jcVlMR1ljaFBwWjNZODlzYTpCbXBaY2FZUF91Tlg0REptNzhHQThHekpmb2th',
            ])
            ->withOptions(["verify"=>false])
            ->asForm()
            ->post('https://wso2-gateway-wso2-system.apps.lab.i3datacenter.my.id/token',[
                "grant_type"=>"password",
                "username"=>"admin",
                "password"=>"admin",
                "scope"=>"apim:subscribe apim:api_key"
            ]);

        $data['generate_token']=$gen_token->object();

        $request->session()->put('access_token', $token->access_token);
        // dd($token->access_token);
        // return response()->json([
        //     'success'=>true,
        //     'data'=>$data,
        //     'message'=>'Ok'
        // ],200);

        // $auth=array(
        //     'username'=>$request
        // );
        // Auth::login((object)$auth);
        return redirect('/main');
    }
    
}
