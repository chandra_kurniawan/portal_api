<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use cebe\openapi\Reader;
use cebe\openapi\spec\OpenAPI;
use cebe\openapi\ReferenceContext;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        $token=$request->session()->get('access_token');
        // dd($token);

        $req = Http::withToken($token)
            ->withoutVerifying()
            // ->withHeaders(['Authorization' => 'bearer c91728b7-a946-3436-ad0e-111a986ebf01'])
            ->withOptions(["verify"=>false])
            ->get('https://wso2-wso2-system.apps.lab.i3datacenter.my.id/api/am/store/v1/apis');

        $data['list_apps']=$req->object();
        // dd($data);

        // return response()->json($data,200);
        return view('admin.home.main',$data);
    }


    public function home(Request $request)
    {

        Reader::readFromJsonFile(public_path('/openapi.json'));
        // dd($openapi);
        // print_r(realpath(public_path().'openapi.json'));
    }

    public function getApps(Request $request,$id)
    {
        $token=$request->session()->get('access_token');
        $req = Http::withToken($token)
            ->withoutVerifying()
            // ->withHeaders(['Authorization' => 'bearer c91728b7-a946-3436-ad0e-111a986ebf01'])
            ->withOptions(["verify"=>false])
            ->get('https://wso2-wso2-system.apps.lab.i3datacenter.my.id/api/am/store/v1/apis/'.$id.'/swagger');

        $data=$req->object();
        $newJsonString = json_encode($data);
        file_put_contents('docs/api-docs.json', $newJsonString);
        return response()->json($data,200);
    }
}
