<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\Admin\HomeController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/welcome', function () {
    return view('admin.home.main');
});

Route::get('/',[AuthController::class,'index'])->name('login');
Route::post('/auth/register',[AuthController::class,'register'])->name('register');

Route::group(['middleware' => 'web'], function() {
    Route::get('/main',[HomeController::class,'index'])->name('main.index');
    Route::get('/home',[HomeController::class,'home'])->name('main.home');
    Route::get('/home/apps/{id}',[HomeController::class,'getApps'])->name('main.getApps');
});
